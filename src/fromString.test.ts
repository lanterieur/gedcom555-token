import * as assert from "assert";
import { tokenizeFromString, checkStringLineTerminators } from "./fromString";
const moduleName = `fromString`;

import {
  TOKENIZE_ERROR,
  LINETERMINATOR_MISSING_ERROR,
  LINETERMINATOR_INCONSISTENT_ERROR,
  LINETEXT_SINGLE_AT_END_ERROR,
  addMsgValues,
} from "./errorMessages";

export default function test() {
  console.log(`\nTesting ${moduleName}.`);

  assert.throws(
    () => checkStringLineTerminators(``),
    new TypeError(LINETERMINATOR_MISSING_ERROR)
  );
  assert.throws(
    () => checkStringLineTerminators(`\n\r\n\r\r\r\r`),
    new TypeError(LINETERMINATOR_INCONSISTENT_ERROR)
  );
  assert.strictEqual(checkStringLineTerminators(`line\nline\nline\n`), `\n`);
  assert.strictEqual(
    checkStringLineTerminators(`line\r\nline\r\nline\r\n`),
    `\r\n`
  );
  assert.strictEqual(checkStringLineTerminators(`line\rline\rline\r`), `\r`);

  assert.throws(
    () => tokenizeFromString(``),
    new TypeError(LINETERMINATOR_MISSING_ERROR)
  );
  assert.throws(
    () => tokenizeFromString(`\n\r\n\r\r\r\r`),
    new TypeError(LINETERMINATOR_INCONSISTENT_ERROR)
  );
  assert.throws(
    () => tokenizeFromString(`0 HEAD\r1 GEDC\r2 VERS 1.2.4@\r0 @12345@ indi\r`),
    new Error(
      addMsgValues(TOKENIZE_ERROR, {
        "%LN%": "2",
        "%MSG%": LINETEXT_SINGLE_AT_END_ERROR,
      })
    )
  );
  assert.deepStrictEqual(
    tokenizeFromString(`0 HEAD\r1 GEDC\r2 VERS 1.2.4\r0 @12345@ indi\r`),
    [
      {
        level: 0,
        tag: `HEAD`,
      },
      {
        level: 1,
        tag: `GEDC`,
      },
      {
        level: 2,
        tag: `VERS`,
        lineItem: `1.2.4`,
      },
      {
        level: 0,
        tag: `INDI`,
        xrefId: `@12345@`,
      },
    ]
  );
  assert.deepStrictEqual(
    tokenizeFromString(`0 HEAD
1 GEDC
2 VERS 5.5.5
2 FORM LINEAGE-LINKED
3 VERS 5.5.5
1 CHAR UTF-8
1 SOUR gedcom.org
0 @U@ SUBM
1 NAME gedcom.org
0 TRLR`),
    [
      {
        level: 0,
        tag: `HEAD`,
      },
      {
        level: 1,
        tag: `GEDC`,
      },
      {
        level: 2,
        tag: `VERS`,
        lineItem: `5.5.5`,
      },
      {
        level: 2,
        tag: `FORM`,
        lineItem: `LINEAGE-LINKED`,
      },
      {
        level: 3,
        tag: `VERS`,
        lineItem: `5.5.5`,
      },
      {
        level: 1,
        tag: `CHAR`,
        lineItem: `UTF-8`,
      },
      {
        level: 1,
        tag: `SOUR`,
        lineItem: `gedcom.org`,
      },
      {
        level: 0,
        tag: `SUBM`,
        xrefId: `@U@`,
      },
      {
        level: 1,
        tag: `NAME`,
        lineItem: `gedcom.org`,
      },
      {
        level: 0,
        tag: `TRLR`,
      },
    ]
  );

  console.log(`${moduleName} test complete.`);
}
