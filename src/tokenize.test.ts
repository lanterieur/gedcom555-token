import * as assert from "assert";
import {
  tokenize,
  validateLevelNumber,
  validateLineValue,
  validateTag,
  validateXrefId,
} from "./tokenize";
import {
  LEVEL_NUMBER_ERROR,
  LINETEXT_SINGLE_AT_END_ERROR,
  LINETEXT_SINGLE_AT_START_ERROR,
  LINETEXT_SINGLE_ERROR,
  LINE_INVALID_ERROR,
  TAG_ERROR,
} from "./errorMessages";
import { tags } from "./tags";
const moduleName = `tokenize`;

export default function test() {
  console.log(`\nTesting ${moduleName}.`);

  assert.throws(() => validateLevelNumber(), new TypeError(LEVEL_NUMBER_ERROR));
  assert.strictEqual(validateLevelNumber(`23`), 23);

  assert.deepStrictEqual(validateLineValue(`23`), {
    pointer: undefined,
    lineItem: `23`,
  });
  assert.deepStrictEqual(validateLineValue(`@23@`), {
    pointer: `@23@`,
    lineItem: undefined,
  });
  assert.throws(
    () => validateLineValue(`Invalid @ line item!`),
    new TypeError(LINETEXT_SINGLE_ERROR)
  );

  assert.strictEqual(validateXrefId(), undefined);
  assert.strictEqual(validateXrefId(`@23@`), `@23@`);

  assert.throws(() => validateTag(`nottag`), new TypeError(TAG_ERROR));
  assert.strictEqual(validateTag(`ABBR`), `ABBR`);

  [
    ``,
    `01 HEAD`,
    `111 HEAD`,
    `11 @REF@`,
    `11 @ref1@ @ref2@ HEAD`,
    `1 @12345678901234567890a@ HEAD`,
  ].forEach((l) =>
    assert.throws(() => tokenize(l), new TypeError(LINE_INVALID_ERROR + l))
  );
  assert.throws(() => tokenize(`0 unkown`), new TypeError(TAG_ERROR));
  assert.throws(
    () => tokenize(`0 HEAD @12345678901234567890a@`),
    new TypeError(LINETEXT_SINGLE_AT_START_ERROR)
  );
  assert.throws(
    () => tokenize(`0 HEAD Invalid line value@`),
    new TypeError(LINETEXT_SINGLE_AT_END_ERROR)
  );
  tags.forEach((t) => {
    assert.deepStrictEqual(tokenize(`1 ${t}`), {
      level: 1,
      tag: t,
    });
    assert.deepStrictEqual(tokenize(`1 @xrefId1@ ${t}`), {
      level: 1,
      tag: t,
      xrefId: `@xrefId1@`,
    });
    assert.deepStrictEqual(tokenize(`1 ${t} @xrefId1@`), {
      level: 1,
      tag: t,
      pointer: `@xrefId1@`,
    });
    assert.deepStrictEqual(tokenize(`1 ${t} @@xrefId1@@`), {
      level: 1,
      tag: t,
      lineItem: `@@xrefId1@@`,
    });
    assert.deepStrictEqual(tokenize(`1 @xrefId1@ ${t} @@xrefId1@@`), {
      level: 1,
      tag: t,
      xrefId: `@xrefId1@`,
      lineItem: `@@xrefId1@@`,
    });
  });

  console.log(`${moduleName} test complete.`);
}
