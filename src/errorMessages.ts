interface IMsgValues {
  [key: string]: string;
}
export function addMsgValues(msg: string, MsgValues: IMsgValues): string {
  return msg.replace(/%\w+%/g, function (all) {
    return MsgValues[all] || all;
  });
}

export const LINE_INVALID_ERROR = "Invalid Gedcom line: ";

export const LEVEL_NUMBER_ERROR =
  "Expected Level Number. The beginning of a new top-level rec" +
  "ord is designated by a line whose level number is 0 (zero)." +
  "\nLevel numbers must be between 0 to 99 and must not contai" +
  "n leading zeroes, for example, level one must be 1, not 01.";

export const LINETERMINATOR_MISSING_ERROR =
  "Expected line terminators (LF,CR or CR+LF).";

export const LINETERMINATOR_INCONSISTENT_ERROR =
  "Inconsistent line terminators. GEDCOM allows different line" +
  " terminators, but each GEDCOM file must use a single line t" +
  "erminator throughout the file.";

const LINETEXT_ERROR_PREFIX = "Invalid Line Text: ";
export const LINETEXT_EMPTY_ERROR = LINETEXT_ERROR_PREFIX + "Empty.";
export const LINETEXT_SINGLE_AT_START_ERROR =
  LINETEXT_ERROR_PREFIX + "Single @ at the start.";
export const LINETEXT_SINGLE_AT_END_ERROR =
  LINETEXT_ERROR_PREFIX + "Single @ at the end.";
export const LINETEXT_SINGLE_ERROR = LINETEXT_ERROR_PREFIX + "Single @.";

export const TAG_ERROR = "Invalid tag.";

export const TOKENIZE_ERROR = "Unable to tokenize line %LN%. %MSG%";
