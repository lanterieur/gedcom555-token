import { Line, Pointer, XrefId } from "./types";
import { Tag, tags } from "./tags";
import {
  LEVEL_NUMBER_ERROR,
  LINE_INVALID_ERROR,
  TAG_ERROR,
} from "./errorMessages";
import {
  POINTER,
  RE_GEDCOM_LINE,
  isLineTextValid,
  isTagValid,
} from "./grammarSyntaxComponents";
const RE_POINTER = new RegExp(`^` + POINTER + `$`);

/**
 * Converts a line from a Gedcom file into a tokenized Line object.
 *
 * @param {String} lineStr Gedcom data line as string.
 * @returns {Line}
 */
export function tokenize(lineStr: string): Line {
  lineStr = lineStr.trimStart();

  const match = lineStr.match(RE_GEDCOM_LINE);
  if (!match) throw new TypeError(LINE_INVALID_ERROR + lineStr);

  const level = validateLevelNumber(match[1]);
  const xrefId = validateXrefId(match[2] as XrefId);
  const tag: Tag = validateTag(match[3]);
  const { pointer, lineItem } = validateLineValue(match[4]);

  let line: Line = {
    level,
    tag,
  };
  if (xrefId) line.xrefId = xrefId;
  if (pointer) line.pointer = pointer;
  if (lineItem) line.lineItem = lineItem;

  return line;
}

export function validateLevelNumber(levelString?: string) {
  // should never happen since the level is a compulsory part of the line regexp
  if (!levelString) throw new TypeError(LEVEL_NUMBER_ERROR);
  return parseInt(levelString);
}

export function validateXrefId(xrefIdString?: XrefId) {
  if (!xrefIdString) return undefined;
  return xrefIdString as XrefId;
}

export function validateTag(tagString?: string) {
  // should never happen since the tag is a compulsory part of the line regexp
  if (!tagString) throw new TypeError(TAG_ERROR);
  tagString = tagString.toUpperCase();
  isTagValid(tagString);
  return tagString as Tag;
}

export function validateLineValue(lineValueString?: string) {
  const res: { pointer: Pointer | undefined; lineItem: string | undefined } = {
    pointer: undefined,
    lineItem: undefined,
  };
  if (lineValueString)
    if (RE_POINTER.test(lineValueString))
      res.pointer = lineValueString as Pointer;
    else {
      isLineTextValid(lineValueString);
      res.lineItem = lineValueString;
    }
  return res;
}
