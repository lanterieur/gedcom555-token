import { tokenizeFromString } from "./fromString";
import * as assert from "assert";
import { tokenize } from "./tokenize";

const VERBOSE = false;
function d (...args:any[]) {
  if(VERBOSE) console.log(...args);
}

/**
 * https://gitlab.com/lanterieur/gedcom555-token/-/issues/1
 */

export default function test (){
console.log(`\nTesting Issue #1.`);
d(`Issue #1 test cases:`);

// Replication test case
let gedcomString = `00 @N0015@ NOTE Some note text.
1 CONT 
1 CONT A third line of note text.`;

// The level has an illegal leading 0.
assert.throws(() => tokenize(gedcomString.split("\n")[0]));
d(`Tokenize throws given the leading 0 on the level.`);

// removing the leading 0
gedcomString = gedcomString.slice(1);
tokenize(gedcomString.split("\n")[0]);
d(`Tokenize does not throw without the leading 0 on the level.`);

// The CONT is followed by an illegal trailing space.
assert.throws(() => tokenizeFromString(gedcomString));
d(`CONT with a trailing space before the terminator is illegal.`);

// removing the trailing space
gedcomString = gedcomString.replace("CONT \n","CONT\n");
tokenizeFromString(gedcomString);
d(`CONT without a trailing space is legal.`);

// Error explanation:
/**
 * The gedcom line is defined as follow:
 * level + [ delim + xref_ID ] + delim + tag + [ delim + line_value ] + terminator
 * where the TAG can be followed by a delim and a value or no delim at all.
 * The trailing space after the CONT tag must either be removed or followed by a line value like a second space.
*/

gedcomString.replace("CONT \n","CONT  \n");
tokenizeFromString(gedcomString);
d(`CONT with a space as line value is legal.`);


console.log(`Issue #1 test complete.`);
}