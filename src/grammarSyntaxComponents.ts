import {
  LINETEXT_EMPTY_ERROR,
  LINETEXT_SINGLE_AT_END_ERROR,
  LINETEXT_SINGLE_AT_START_ERROR,
  LINETEXT_SINGLE_ERROR,
  TAG_ERROR,
} from "./errorMessages";
import { tags } from "./tags";
// character patterns

export const SPACE = ` `;
/**
delim:=
space
where:
space = U+0020, the Space character
The explicit inclusion of the delim definition as the space character is no mistake. It stresses
that the delimiter is a space character and nothing else.
 */
export const DELIM = SPACE;
/**
alpha:=
[U+0041 - U+005A | U+0061 - U+007A ]
where:
U+0041 - U+005A = A through Z
U+0061 - U+007A= a through z
 */
export const ALPHA_CHAR = `A-Za-z`;
/* 
digit:=
U+0030 - U+0039
where:
U+0030 - U+0039 = One of the digits 0, 1,2,3,4,5,6,7,8,9
*/
export const DIGIT_CHAR = `0-9`;
/* 
non_zero_digit:=
U+0031 - U+0039
where:
U+0031 - U+0039 = One of the digits 1,2,3,4,5,6,7,8,9
*/
export const NON_DIGIT_CHAR = `1-9`;
/**
alphanum:=
[ alpha | digit ]
 */
export const ALPHANUM_CHAR = ALPHA_CHAR + DIGIT_CHAR;
/* 
line_char:=
all legal characters with some exceptions and one special case
specifically disallowed:
U+0000 - U+001F, except U+0009 = most C0 control characters
U+00FF = Delete character
specifically allowed:
U+0009 = Horizontal Tab
special case:
U+0040 + U+0040 = @@
Most control characters in the range U+0000 - U+001F (C0 Control Set) are disallowed. The
one exception is U+0009, the Horizontal Tab. Text may contain horizontal tabs.
Note that Carriage Return (U+000D) and Line Feed (U+000A) must not occur inside text.
Line breaks are supported through the CONT record. The use of the space character ( ),
number-sign (#) and underscore (_), is perfectly legal in text.
Legal Characters
All text must abide by the rules of the character set used. The character set used determines
which code points are legal. The character set used determines which code point
combinations are legal. Use string functions to make sure that any line breaks are between
characters, not inside characters.
Special Case: At sign (@)
The At sign (@) is legal in text, but because of its special function in GEDCOM, there's a
twist. Text is not allowed to contain a single at sign (@), but must always include two
consecutive at signs (@@) instead, to clearly distinguish an at sign that is just text from an
at sign starting a cross-reference or escape sequence.
This is similar to backward slashes (\) having to be doubled inside C and C++ text strings.
This at sign rule affects all textual line values, but particularly the EMAIL line value,
<ADDRESS_EMAIL>, introduced with GEDCOM 5.5.1, as email addresses always contain
a single at sign. Within a GEDCOM file, that single at sign must be represented by a double
at sign.
A GEDCOM writer must export a double at sign instead of single at sign, and a GEDCOM
reader must import each double at sign as a single one. Exporting a single at sign where a
double at sign should be exported is a serious GEDCOM grammar mistake. A GEDCOM
5.5.5 reader must not interpret a single at sign as if it were a double at sign, but must report a
fatal error and exit.
dates
GEDCOM writers should pay special attention to at signs in date fields. When an application
does not provide support for a particular calendar, a user may choose to enter a date
complete with the <DATE_CALENDAR_ESCAPE> for the calendar, e.g. “@#JULIAN@
12 Oct 1492” (without the quotes). The at signs within such dates should not be doubled.
Not Deprecated
This feature complicates GEDCOM handling, so it is desirable to mark this GEDCOM
feature as deprecated, and eventually get rid of it, but as long as some records can contain
GEDCOM pointers as well as free text, this feature allows GEDCOM readers to distinguish
between an at sign that starts a pointer (single at sign), and one that is merely an at sign in
some text (double at sign).
*/
export const LINE_CHAR = `^\\n\\r`;

// composite patterns

/* 
line_text:=
[ line_char | line_text + line_char ]
line_text is text that neither is nor contains a pointer or an escape sequence.
*/
export const LINE_TEXT = `[${LINE_CHAR}]+`;
const INVALID_LINE_TEXT_RE_1 = /^@(?:[^@].*)?$/;
const INVALID_LINE_TEXT_RE_2 = /^(?:.*[^@])?@$/;
const INVALID_LINE_TEXT_RE_3 = /[^@]@[^@]/;

/* 
level:=
[ digit | non_zero_digit + digit ]
The level number consists of one or two digits. It may be zero, but must not start with
leading zeroes. 
*/
export const LEVEL_NUMBER = `[${NON_DIGIT_CHAR}]?[${DIGIT_CHAR}]`;
/**
escape_text:=
[ alphanum | escape_text + alphanum | escape_text + space ]
where:
space = U+0020, the Space character
The escape_text is the part of an escape between the opening and closing at sign (@).
Escape sequences are defined by a GEDCOM form for that GEDCOM form.
GEDCOM 5.5.1 allows almost any Unicode character inside escape sequences. GEDCOM
5.5.5 restricts escape sequences to alphanumerical characters and the space character.
Notice that escape_text must start with an alphanumerical character, that it must not start
with a space.
The space character is allowed but deprecated; it is only included to keep the already
existing escape sequence @#DFRENCH R@ legal.
 */
export const ESCAPE_TEXT = `[${ALPHANUM_CHAR}][${ALPHANUM_CHAR}${SPACE}]*`;
/* 
escape:=
U+0040 + U+0023 + escape_text + U+0040
where:
U+0040 = @
U+0023 = #
A GEDCOM escape sequence begins and ends with an at sign.
An escape sequence must not contain any at sign and must not be followed by an at sign; an
escape sequence must be followed by either a delim (the space character) or a terminator.
*/
export const ESCAPED_TEXT = `@#[${ESCAPE_TEXT}@`;
/*
identifier_string:=
[ alphanum | alphanum + identifier_string ]
*/
export const IDENTIFIER_STRING = `[${ALPHANUM_CHAR}]{1,20}`;
/* 
line_item:=
[ escape | line_text | escape + delim + line_text ]
This line_item definition is different from the GEDCOM 5.5.1 definition. It explicitly
restricts line_item to just three different formats.
The explicit inclusion of nothing but an escape sequence as a possible line value is as
intended. It has been used in GEDCOM 5.0, it is deprecated now.
*/
export const LINE_ITEM = `(?:${ESCAPED_TEXT}|${LINE_TEXT}|(?:${ESCAPED_TEXT}${DELIM}${LINE_TEXT}))`;
/* 
tag:=
[ [ U+005F ] + alphanum | tag + alphanum ]
where:
U+005F = _ (underscore)
GEDCOM tags are Case-Sensitive.
Each tag has a single correct casing.
All Basic GEDCOM Language tags are UPPERCASE.
▪ A GEDCOM writer must use the correct casing for tags.
▪ A GEDCOM reader must recognise tags through case-sensitive comparison.
▪ A GEDCOM reader must not recognise tags through case-insensitive comparison.
GEDCOM tags are generally restricted to alphanumeric characters. Tags need not start with
a letter, but may start with a digit. Tags may additionally start with an underscore; this is
allowed because the Lineage-Linked Form demands that non-standard tags start with an
underscore
 */
export const TAG = `_?[${ALPHANUM_CHAR}][${ALPHANUM_CHAR}_]*`;
/* 
xref_ID:=
at + identifier_string + at
where:
at = U+0040, the At Sign (@)
GEDCOM 5.5.5 demands that the identifier_string be alphanumerical
*/
export const XREF_ID = `@${IDENTIFIER_STRING}@`;
/* 
pointer:=
xref_ID
*/
export const POINTER = XREF_ID;
/* 
line_value:=
[ pointer | line_item ]
*/
export const LINE_VALUE = `(?:${POINTER}|${LINE_ITEM})`;

// patterns

// level + [ delim + xref_ID ] + delim + tag + [ delim + line_value ] + terminator
export const RE_GEDCOM_LINE = new RegExp(
  `^(${LEVEL_NUMBER})(?:${DELIM}(${XREF_ID}))?${DELIM}(${TAG})(?:${DELIM}(${LINE_VALUE}))?$`
);

// Validation functions

export function isLineTextValid(str: string, emptyIsInvalid = false) {
  if (!str?.length && emptyIsInvalid) throw new TypeError(LINETEXT_EMPTY_ERROR);

  // avoid ^@...$
  if (INVALID_LINE_TEXT_RE_1.test(str))
    throw new TypeError(LINETEXT_SINGLE_AT_START_ERROR);

  // avoid ^...@$
  if (INVALID_LINE_TEXT_RE_2.test(str))
    throw new TypeError(LINETEXT_SINGLE_AT_END_ERROR);

  // avoid ...@...
  if (str.length > 3 && INVALID_LINE_TEXT_RE_3.test(str))
    throw new TypeError(LINETEXT_SINGLE_ERROR);

  return true;
}

export function isTagValid(str: string) {
  if (!tags.includes(str)) throw new TypeError(TAG_ERROR);
  return true;
}
