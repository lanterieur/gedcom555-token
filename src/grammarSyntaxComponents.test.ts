import * as assert from "assert";
import { tags } from "./tags";
import {
  isLineTextValid,
  isTagValid,
  RE_GEDCOM_LINE,
} from "./grammarSyntaxComponents";
import {
  LINETEXT_EMPTY_ERROR,
  LINETEXT_SINGLE_AT_END_ERROR,
  LINETEXT_SINGLE_AT_START_ERROR,
  LINETEXT_SINGLE_ERROR,
  TAG_ERROR,
} from "./errorMessages";

export default function test() {
  console.log(`\nTesting grammarSyntaxComponents.`);
  const terr = new TypeError(TAG_ERROR);
  assert.throws(() => isTagValid(null!), terr);
  assert.throws(() => isTagValid(`toodamnlong`), terr);
  assert.throws(() => isTagValid(`notæ`), terr);
  assert.throws(() => isTagValid(`yolo`), terr);
  tags.forEach((t) => assert(isTagValid(t)));

  assert.throws(
    () => isLineTextValid(null!, true),
    new TypeError(LINETEXT_EMPTY_ERROR)
  );
  assert.throws(
    () => isLineTextValid(1 as unknown as string, true),
    new TypeError(LINETEXT_EMPTY_ERROR)
  );
  assert.throws(
    () => isLineTextValid(``, true),
    new TypeError(LINETEXT_EMPTY_ERROR)
  );
  assert.throws(
    () => isLineTextValid(`@`),
    new TypeError(LINETEXT_SINGLE_AT_START_ERROR)
  );
  assert.throws(
    () => isLineTextValid(`@bcd`),
    new TypeError(LINETEXT_SINGLE_AT_START_ERROR)
  );
  assert.throws(
    () => isLineTextValid(`nsiun@`),
    new TypeError(LINETEXT_SINGLE_AT_END_ERROR)
  );
  assert.throws(
    () => isLineTextValid(`euff@bcd`),
    new TypeError(LINETEXT_SINGLE_ERROR)
  );
  isLineTextValid(``);
  isLineTextValid(`auAUIE uinsa éèy@@ ()()324243543"<>"*+°_/`);
  isLineTextValid(1 as unknown as string);

  assert.strictEqual(RE_GEDCOM_LINE.test(``), false);
  assert.strictEqual(RE_GEDCOM_LINE.test(`123 tagname`), false);
  assert.strictEqual(RE_GEDCOM_LINE.test(`03 tagname`), false);
  // only tag
  assert(RE_GEDCOM_LINE.test(`0 anyAlphaNum123`));
  tags.forEach((t) =>
    assert(
      RE_GEDCOM_LINE.test(`0 ${t}`),
      `RE_GEDCOM_LINE should match "0 ${t}".`
    )
  );
  // with xrefid
  assert.strictEqual(
    RE_GEDCOM_LINE.test(`0 @xrefid1_invalid@ anyAlphaNum123`),
    false
  );
  assert(RE_GEDCOM_LINE.test(`0 @xrefid1@ anyAlphaNum123`));
  // with value
  assert(RE_GEDCOM_LINE.test(`0 anyAlphaNum123 @xrefid1@`));
  assert(
    RE_GEDCOM_LINE.test(
      `0 anyAlphaNum123 Actually anything is valid here @@ 12 "<>()@+-/*°€_`
    )
  );
  // invalid pointer validates as line item but without the single "@" validation.
  // use isLineTextValid to validate
  assert(RE_GEDCOM_LINE.test(`0 anyAlphaNum123 @xrefid1invalid!@`));
  // with xrefid and value
  assert(RE_GEDCOM_LINE.test(`0 @xrefid1@ anyAlphaNum123 @xrefid1@`));
  assert(
    RE_GEDCOM_LINE.test(
      `0 @xrefid1@ anyAlphaNum123 Actually anything is valid here @@ 12 "<>()@+-/*°€_`
    )
  );
  {
    const str = `0 anyAlphaNum123`;
    const matchLine = str.match(RE_GEDCOM_LINE);
    assert(!!matchLine);
    assert.strictEqual(matchLine[0], str);
    assert.strictEqual(matchLine[1], `0`);
    assert.strictEqual(matchLine[2], undefined);
    assert.strictEqual(matchLine[3], `anyAlphaNum123`);
    assert.strictEqual(matchLine[4], undefined);
  }
  {
    const str = `0 @xrefid1@ anyAlphaNum123`;
    const matchLine = str.match(RE_GEDCOM_LINE);
    assert(!!matchLine);
    assert.strictEqual(matchLine[0], str);
    assert.strictEqual(matchLine[1], `0`);
    assert.strictEqual(matchLine[2], `@xrefid1@`);
    assert.strictEqual(matchLine[3], `anyAlphaNum123`);
    assert.strictEqual(matchLine[4], undefined);
  }
  {
    const str = `0 anyAlphaNum123 Actually anything is valid here @@ 12 "<>()@+-/*°€_`;
    const matchLine = str.match(RE_GEDCOM_LINE);
    assert(!!matchLine);
    assert.strictEqual(matchLine[0], str);
    assert.strictEqual(matchLine[1], `0`);
    assert.strictEqual(matchLine[2], undefined);
    assert.strictEqual(matchLine[3], `anyAlphaNum123`);
    assert.strictEqual(
      matchLine[4],
      `Actually anything is valid here @@ 12 "<>()@+-/*°€_`
    );
  }
  {
    const str = `0 @xrefid1@ anyAlphaNum123 Actually anything is valid here @@ 12 "<>()@+-/*°€_`;
    const matchLine = str.match(RE_GEDCOM_LINE);
    assert(!!matchLine);
    assert.strictEqual(matchLine[0], str);
    assert.strictEqual(matchLine[1], `0`);
    assert.strictEqual(matchLine[2], `@xrefid1@`);
    assert.strictEqual(matchLine[3], `anyAlphaNum123`);
    assert.strictEqual(
      matchLine[4],
      `Actually anything is valid here @@ 12 "<>()@+-/*°€_`
    );
  }
  console.log(`grammarSyntaxComponents test complete.`);
}
