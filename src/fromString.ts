import {
  LINETERMINATOR_INCONSISTENT_ERROR,
  LINETERMINATOR_MISSING_ERROR,
  TOKENIZE_ERROR,
  addMsgValues,
} from "./errorMessages";
import { tokenize } from "./tokenize";
import { Line } from "./types";

/**
 * Converts the raw data (all lines) from a Gedcom file into an array of tokenized Line objects.
 * @param {String} str The raw data of a Gedcom file as a string.
 * @returns {Line[]}
 */
export function tokenizeFromString(str: string) {
  const LT = checkStringLineTerminators(str);
  const rawLines = str.split(LT).filter((str) => str.trim());
  const lines: Line[] = [];
  let currentLineNumber = -1;
  for (const rawLine of rawLines)
    try {
      currentLineNumber++;
      lines.push(tokenize(rawLine));
    } catch (err) {
      if (err instanceof TypeError) {
        throw new Error(
          addMsgValues(TOKENIZE_ERROR, {
            "%LN%": String(currentLineNumber),
            "%MSG%": err.message,
          })
        );
      } else throw err;
    }
  return lines;
}

export function checkStringLineTerminators(str: string) {
  const lts = new Set(str.match(/([\r\n]{1,2})/g) || []);
  if (!lts.size) throw new TypeError(LINETERMINATOR_MISSING_ERROR);
  else if (lts.size > 1) throw new TypeError(LINETERMINATOR_INCONSISTENT_ERROR);
  return lts.values().next().value;
}
