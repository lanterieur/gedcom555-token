import { Tag } from "./tags";

export type XrefId = `@${string}@`;
export type Pointer = XrefId;

export type Line = {
  level: number;
  tag: Tag;
  xrefId?: XrefId;
  pointer?: Pointer;
  lineItem?: string;
};
