import error from "./errorMessages.test";
import fromString from "./fromString.test";
import grammar from "./grammarSyntaxComponents.test";
import tokenize from "./tokenize.test";
import issue1 from "./issue-001-empty-cont.test";

error();
grammar();
tokenize();
fromString();
issue1();