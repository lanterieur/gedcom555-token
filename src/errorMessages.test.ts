import * as assert from "assert";
import { addMsgValues } from "./errorMessages";
const moduleName = `errorMessages`;

import { TOKENIZE_ERROR } from "./errorMessages";

export default function test() {
  console.log(`\nTesting ${moduleName}.`);

  assert.strictEqual(addMsgValues(``, {}), ``);
  assert.strictEqual(addMsgValues(`yolo`, {}), `yolo`);
  assert.strictEqual(
    addMsgValues(`%yolo%`, { "%yolo%": `bambino` }),
    `bambino`
  );
  assert.strictEqual(
    addMsgValues(`%rolo% %yolo% %yolo%`, {
      "%rolo%": `%yolo%`,
      "%yolo%": `bambino`,
    }),
    `%yolo% bambino bambino`
  );
  assert.strictEqual(
    addMsgValues(TOKENIZE_ERROR, { "%LN%": "2", "%MSG%": `Something` }),
    "Unable to tokenize line 2. Something"
  );

  console.log(`${moduleName} test complete.`);
}
